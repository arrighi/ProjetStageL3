#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/time.h>   // TicToc

typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

bool next_iteration = false;

void print4x4Matrix (const Eigen::Matrix4d & matrix);
void keyboardEventOccurred (const pcl::visualization::KeyboardEvent& event, void* nothing);
float mid_distance(PointCloudT::Ptr cloud);
float bounding_box(PointCloudT::Ptr cloud);
int load_param(std::string file_param, float& fx, float& fy, float& cx, float cy);
int load_depth_map(std::string const& file_depthmap, std::string const& param, 
               PointCloudT::Ptr const& cloud, float threshold, float thresholdLeft, float thresholdRight);
void help();

int main (int argc, char* argv[])
{
  // The point clouds we will be using
  PointCloudT::Ptr cloud_in (new PointCloudT);  // Original point cloud
  PointCloudT::Ptr cloud_tr (new PointCloudT);  // Transformed point cloud
  PointCloudT::Ptr cloud_icp (new PointCloudT);  // ICP output point cloud

  // Program arguments
  std::string source_file, source_parameter; //point cloud or depthmap source file and parameter for depthmap
  std::string target_file, target_parameter; //point cloud or depthmap target file and parameter for depthmap
  bool transforme(false); // Transforme ce source point cloud
  int iterations = INT_MAX;  // Default number of ICP iterations
  float MaxCorrespondenceDistance(0.);
  float EuclideanFitnessEpsilon(0.);
  pcl::console::TicToc time;
  float threshold(0.);
  float thresholdLeft(0.);
  float thresholdRight(0.);

  // Checking program arguments
  // Add here for new argument
  for(int argI = 1; argI < argc; ++argI)
  {
    const std::string arg(argv[argI]);

    // Target point cloud file
    if(arg == "-target")
    {
      target_file = argv[++argI];
      if(target_file.substr(target_file.length()-4) == ".txt")
      {
        target_parameter = arg[++argI];
      }
    }

    // Source point cloud file
    else if(arg == "-source")
    {
      source_file = argv[++argI];
      if(source_file.substr(source_file.length()-4) == ".txt")
      {
        source_parameter = arg[++argI];
      }
    }

   // Number of itération for the ICP
    else if(arg == "-i")
    {
      iterations = atoi(argv[++argI]);
      if(iterations < 1)
      {
        PCL_ERROR("Number of initial iterations must be >= 1 \n");
        return(-1);
      }
    }

    // Set MaxCorrespondenceDistance
    else if (arg == "-mcd")
    {
      MaxCorrespondenceDistance = atof(argv[++argI]);
    }

    // Set EuclideanFitnessEpsilon
    else if (arg == "-efe")
    {
      EuclideanFitnessEpsilon = atof(argv[++argI]);
    }

    // Set transformation
    else if (arg == "-transforme")
    {
      transforme = true;
    }

    // Threshold for depthmap
    else if(arg == "-threshold")
    {
      threshold = atof(argv[++argI]);
    }

    // Threshold for depthmap
    else if(arg == "-thresholdLeft")
    {
      thresholdLeft = atof(argv[++argI]);
    }

    // Threshold for depthmap
    else if(arg == "-thresholdRight")
    {
      thresholdRight = atof(argv[++argI]);
    }

    // Help
    else if(arg == "-h")
    {
      help();
      return(1);
    }
    
    else
    {
      std::cout << "Unknown argument: " << arg << std::endl;
    }
  }

  //Load source point cloud
  if(source_file.substr(source_file.length()-4) == ".ply")
  {
    time.tic();
    if(pcl::io::loadPLYFile(source_file, *cloud_tr) < 0)
    {
      PCL_ERROR("Error loading cloud %s. \n", source_file.c_str());
      return(-1);
    }
    std::cout << "\nLoaded source_file " << source_file << " (" << cloud_tr->size () << " points) in " << time.toc () << " ms\n" << std::endl;
  }
  else if(source_file.substr(source_file.length()-4) == ".txt")
  {
    time.tic();
    if(load_depth_map(source_file, source_parameter, cloud_tr, threshold, thresholdLeft, thresholdRight) != 0)
    {
      PCL_ERROR("Error loading cloud %s. \n", source_file.c_str());
      return(-1);
    }
    std::cout << "\nLoaded source_file " << source_file << " (" << cloud_tr->size () << " points) in " << time.toc () << " ms\n" << std::endl;
  }
  else
  {
     std::cout << "Error extention source_file unknown: " << source_file.substr(source_file.length()-4) << std::endl;
     return(-1);
  }

  //Load target point cloud
  if(target_file.substr(target_file.length()-4) == ".ply")
  {
    time.tic();
    if(pcl::io::loadPLYFile(target_file, *cloud_in) < 0)
    {
      PCL_ERROR("Error loading cloud %s. \n", target_file.c_str());
      return(-1);
    }
    std::cout << "\nLoaded target_file " << target_file << " (" << cloud_in->size () << " points) in " << time.toc () << " ms\n" << std::endl;
  }
  else if(target_file.substr(target_file.length()-4) == ".txt")
  {
    time.tic();
    if(load_depth_map(target_file, target_parameter, cloud_in, threshold, thresholdLeft, thresholdRight) != 0)
    {
      PCL_ERROR("Error loading cloud %s. \n", target_file.c_str());
      return(-1);
    }
    std::cout << "\nLoaded target_file " << target_file << " (" << cloud_in->size () << " points) in " << time.toc () << " ms\n" << std::endl;
  }
  else
  {
     std::cout << "Error extention target_file unknown: " << target_file.substr(target_file.length()-4) << std::endl;
     return(-1);
  }


  // Defining a rotation matrix and translation vector
  Eigen::Matrix4d transformation_matrix = Eigen::Matrix4d::Identity ();
  
  if(transforme)
  {
    // A rotation matrix (see https://en.wikipedia.org/wiki/Rotation_matrix)
    double theta = M_PI / 16;  // The angle of rotation in radians
    transformation_matrix (0, 0) = cos (theta);
    transformation_matrix (0, 1) = -sin (theta);
    transformation_matrix (1, 0) = sin (theta);
    transformation_matrix (1, 1) = cos (theta);

    // A translation on Z axis (0.4 meters)
    transformation_matrix (2, 3) = 0.;

    // Display in terminal the transformation matrix
    std::cout << "Applying this rigid transformation to: cloud_in -> cloud_icp" << std::endl;
    print4x4Matrix (transformation_matrix);

    // Executing the transformation
    pcl::transformPointCloud (*cloud_tr, *cloud_icp, transformation_matrix);
    *cloud_tr = *cloud_icp;  // We backup cloud_icp into cloud_tr for later use
  }
  else
  {
    *cloud_icp = *cloud_tr;  // We keep dont want to change cloud_tr  for later use
  }
 
  // Standardization of point cloud
  double bbox = std::max(bounding_box(cloud_in), bounding_box(cloud_icp));
  transformation_matrix = Eigen::Matrix4d::Identity();
  transformation_matrix(0,0) = 1./bbox;
  transformation_matrix(1,1) = 1./bbox;
  transformation_matrix(2,2) = 1./bbox;
  print4x4Matrix (transformation_matrix);
  pcl::transformPointCloud(*cloud_tr, *cloud_icp, transformation_matrix);
  *cloud_tr = *cloud_in;
  pcl::transformPointCloud(*cloud_tr, *cloud_in, transformation_matrix);
  *cloud_tr = *cloud_icp;
  

  // The Iterative Closest Point algorithm
  time.tic ();
  pcl::IterativeClosestPoint<PointT, PointT> icp;
  icp.setMaximumIterations (iterations);
  icp.setInputSource (cloud_icp);
  icp.setInputTarget (cloud_in);
  if (MaxCorrespondenceDistance > 0.)
  {
    icp.setMaxCorrespondenceDistance (MaxCorrespondenceDistance); 
  }
  if (EuclideanFitnessEpsilon > 0.)
  {
    icp.setEuclideanFitnessEpsilon (EuclideanFitnessEpsilon); 
  }
  else
  {
    icp.setEuclideanFitnessEpsilon (std::min(mid_distance(cloud_in), mid_distance(cloud_icp))/20);
  }
  icp.align (*cloud_icp);
  icp.setMaximumIterations (1);  // We set this variable to 1 for the next time we will call .align () function
  std::cout << "Applied " << iterations << " ICP iteration(s) in " << time.toc () << " ms" << std::endl;

  if (icp.hasConverged ())
  {
    std::cout << "\nICP has converged, score is " << icp.getFitnessScore () << std::endl;
    std::cout << "\nICP transformation " << iterations << " : cloud_icp -> cloud_in" << std::endl;
    transformation_matrix = icp.getFinalTransformation ().cast<double>();
    print4x4Matrix (transformation_matrix);
  }
  else
  {
    PCL_ERROR ("\nICP has not converged.\n");
    return (-1);
  }

  // Visualization
  pcl::visualization::PCLVisualizer viewer ("ICP");
  // Create two verticaly separated viewports
  int v1 (0);
  int v2 (1);
  viewer.createViewPort (0.0, 0.0, 0.5, 1.0, v1);
  viewer.createViewPort (0.5, 0.0, 1.0, 1.0, v2);

  // The color we will be using
  float bckgr_gray_level = 0.0;  // Black
  float txt_gray_lvl = 1.0 - bckgr_gray_level;

  // Original point cloud is white
  pcl::visualization::PointCloudColorHandlerCustom<PointT> cloud_in_color_h (cloud_in, (int) 255 * txt_gray_lvl, (int) 255 * txt_gray_lvl,
                                                                             (int) 255 * txt_gray_lvl);
  viewer.addPointCloud (cloud_in, cloud_in_color_h, "cloud_in_v1", v1);
  viewer.addPointCloud (cloud_in, cloud_in_color_h, "cloud_in_v2", v2);

  // Transformed point cloud is green
  pcl::visualization::PointCloudColorHandlerCustom<PointT> cloud_tr_color_h (cloud_tr, 20, 180, 20);
  viewer.addPointCloud (cloud_tr, cloud_tr_color_h, "cloud_tr_v1", v1);

  // ICP aligned point cloud is red
  pcl::visualization::PointCloudColorHandlerCustom<PointT> cloud_icp_color_h (cloud_icp, 180, 20, 20);
  viewer.addPointCloud (cloud_icp, cloud_icp_color_h, "cloud_icp_v2", v2);

  // Adding text descriptions in each viewport
  viewer.addText ("White: Original point cloud\nGreen: Matrix transformed point cloud", 10, 15, 16, txt_gray_lvl, txt_gray_lvl, txt_gray_lvl, "icp_info_1", v1);
  viewer.addText ("White: Original point cloud\nRed: ICP aligned point cloud", 10, 15, 16, txt_gray_lvl, txt_gray_lvl, txt_gray_lvl, "icp_info_2", v2);

  std::stringstream ss;
  ss << iterations;
  std::string iterations_cnt = "ICP iterations = " + ss.str ();
  viewer.addText (iterations_cnt, 10, 60, 16, txt_gray_lvl, txt_gray_lvl, txt_gray_lvl, "iterations_cnt", v2);

  // Set background color
  viewer.setBackgroundColor (bckgr_gray_level, bckgr_gray_level, bckgr_gray_level, v1);
  viewer.setBackgroundColor (bckgr_gray_level, bckgr_gray_level, bckgr_gray_level, v2);

  // Set camera position and orientation
  viewer.setCameraPosition (-3.68332, 2.94092, 5.71266, 0.289847, 0.921947, -0.256907, 0);
  viewer.setSize (1280, 1024);  // Visualiser window size

  // Register keyboard callback :
  viewer.registerKeyboardCallback (&keyboardEventOccurred, (void*) NULL);

  // Display the visualiser
  while (!viewer.wasStopped ())
  {
    viewer.spinOnce ();

    // The user pressed "space" :
    if (next_iteration)
    {
      // The Iterative Closest Point algorithm
      time.tic ();
      icp.align (*cloud_icp);
      std::cout << "Applied 1 ICP iteration in " << time.toc () << " ms" << std::endl;

      if (icp.hasConverged ())
      {
        printf ("\033[11A");  // Go up 11 lines in terminal output.
        printf ("\nICP has converged, score is %+.0e\n", icp.getFitnessScore ());
        std::cout << "\nICP transformation " << ++iterations << " : cloud_icp -> cloud_in" << std::endl;
        transformation_matrix *= icp.getFinalTransformation ().cast<double>();  // WARNING /!\ This is not accurate! For "educational" purpose only!
        print4x4Matrix (transformation_matrix);  // Print the transformation between original pose and current pose

        ss.str ("");
        ss << iterations;
        std::string iterations_cnt = "ICP iterations = " + ss.str ();
        viewer.updateText (iterations_cnt, 10, 60, 16, txt_gray_lvl, txt_gray_lvl, txt_gray_lvl, "iterations_cnt");
        viewer.updatePointCloud (cloud_icp, cloud_icp_color_h, "cloud_icp_v2");
      }
      else
      {
        PCL_ERROR ("\nICP has not converged.\n");
        return (-1);
      }
    }
    next_iteration = false;
  }
  return (0);
}

void print4x4Matrix (const Eigen::Matrix4d & matrix)
{
  printf ("Rotation matrix :\n");
  printf ("    | %6.3f %6.3f %6.3f | \n", matrix (0, 0), matrix (0, 1), matrix (0, 2));
  printf ("R = | %6.3f %6.3f %6.3f | \n", matrix (1, 0), matrix (1, 1), matrix (1, 2));
  printf ("    | %6.3f %6.3f %6.3f | \n", matrix (2, 0), matrix (2, 1), matrix (2, 2));
  printf ("Translation vector :\n");
  printf ("t = < %6.3f, %6.3f, %6.3f >\n\n", matrix (0, 3), matrix (1, 3), matrix (2, 3));
}

void keyboardEventOccurred (const pcl::visualization::KeyboardEvent& event,
                       void* nothing)
{
  if (event.getKeySym () == "space" && event.keyDown ())
    next_iteration = true;
}

float mid_distance(PointCloudT::Ptr cloud)
{
  float sum(0);
 
  pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
  kdtree.setInputCloud (cloud); 
  std::vector<int> plop;
  std::vector<float> distance;

  for(PointCloudT::iterator it = cloud->begin(); it != cloud->end(); ++it)
  {
    kdtree.nearestKSearch(*it, 2, plop, distance);
    sum += distance[1];
  }
  return sum / cloud->size();
}

float bounding_box(PointCloudT::Ptr cloud)
{
  PointT tmp(cloud->back());
  float xmin(tmp.x), ymin(tmp.y), zmin(tmp.z), xmax(tmp.x), ymax(tmp.y), zmax(tmp.z);
  for(PointCloudT::iterator it = cloud->begin(); it != cloud->end(); ++it)
  {
    if(xmin > it->x){xmin = it->x;}
    if(ymin > it->y){ymin = it->y;}
    if(zmin > it->z){zmin = it->z;}
    if(xmax < it->x){xmax = it->x;}
    if(ymax < it->y){ymax = it->y;}
    if(zmax < it->z){zmax = it->z;}
  }

  return sqrt(pow(xmax-xmin,2)+pow(ymax-ymin,2)+pow(zmax-zmin,2));
}

int load_param(std::string file_param, float& fx, float& fy, float& cx, float cy){
  std::ifstream param(file_param.c_str());
  if(param.is_open())
  {
    for(int i(0); i<4; ++i)
    {
      std::string name;
      param >> name;
      if(name == "fx")
      {
        param >> fx; 
      }
      if(name == "fy")
      {
        param >> fy; 
      }
      if(name == "cx")
      {
        param >> cx; 
      }
      if(name == "cy")
      {
        param >> cy; 
      }
    }
    return(0);
  }
  else
  {
    std::cout << "Error loading parameter: " << file_param << std::endl;
    return(-1);
  }
}

int load_depth_map(std::string const& file_depthmap, std::string const& param, 
          PointCloudT::Ptr const& cloud, float threshold, float thresholdLeft, float thresholdRight){
   float fx(365.166), fy(365.166), cx(259.653), cy(206.166);
   //load_param(param, fx, fy, cx, cy);

   std::ifstream depthmap(file_depthmap.c_str());
   if(depthmap.is_open())
   {
      std::string line;
      int height(0), width(0), counter(0);
      while(getline(depthmap, line))
      {
         std::istringstream is_before(line);
         width = 0;
         for(std::istream_iterator<float> it = std::istream_iterator<float>(is_before); it !=  std::istream_iterator<float>(); ++it)
         {
           if(((*it)< threshold || threshold == 0.) && 
                     (width < thresholdRight || thresholdRight == 0.) &&
                     (width > thresholdLeft || thresholdLeft == 0.)){ 
               PointT point3d;
               point3d.x = (width-cx)*(*it)/fx;
               point3d.y =(cy-height)*(*it)/fy;
               point3d.z = (*it);
               cloud->points.push_back(point3d);++counter;}
            width++;
         }
         height++;
      }
      cloud->header.frame_id = file_depthmap.substr(0,file_depthmap.find("."));
      cloud->height = 1;
      cloud->width = counter;
      cloud->is_dense = true;
      cloud->points.resize(counter);
      
      return 0;
   }
   else
   {
      return 1;
   }
}

void help()
{
  std::cout << "Usage: interactive_icp [ OPTIONS ] -target file [intrinsic parameter]" << std::endl;
  std::cout << "                                   -source file [intrinsic parameter]" << std::endl;
  std::cout << std::endl;
  std::cout << "Options supported by interactive_icp:" << std::endl;
  std::cout << "  [intrinsic parameter] : Only for depth map" << std::endl;
  std::cout << "  -i N" << std::endl;
  std::cout << "   ICP will do at most N iterations. Default INT_MAX." << std::endl;
  std::cout << "  -mcd F " << std::endl;
  std::cout << "   Set the maximum distance threshold between two correspondent points in source <-> target. Default no limitation." << std::endl;
  std::cout << "   For identical point cloud, it work, for point cloud with some overlap, it need to be set." << std::endl;
  std::cout << "  -efe" << std::endl;
  std::cout << "   Set the maximum allowed Euclidean error between two consecutive steps in the ICP loop, before the algorithm is considered to have converged. Default distance moyenne entre 2 voisins." << std::endl;
  std::cout << "  -transforme" << std::endl;
  std::cout << "   Apply a linear transformation to the source point cloud (rotation PI/8). Defautl no transformation" << std::endl;
  std::cout << "  -threshold" << std::endl;
  std::cout << "   Only for depth map. Only point before threshold will be import. Default no threshold." << std::endl;
  std::cout << "  -thresholdLef, threholdRight" << std::endl;
  std::cout << "   Only for depth map. Limite the width of the depth map. Defautl no threshold." << std::endl;
  std::cout << "  -h" << std::endl;
  std::cout << "   Help" << std::endl;
  std::cout << std::endl;
}
